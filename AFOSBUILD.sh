bash autogen.sh

./configure

make

make install

cp -Rf tools/sadb2conf.awk /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
